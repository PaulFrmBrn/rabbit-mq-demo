package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 22.03.15.
 */
public class Producer {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException {


        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
                                                         // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        channel.queueDeclare(QUEUE_NAME,false,false,false,null);  // declaring the queue

        String message = "Hello world!";

        channel.basicPublish("",QUEUE_NAME,null,message.getBytes()); // publishing the message
        System.out.println(" [x] Sent '" + message + "'");


        channel.close();
        connection.close();

        System.out.println("producer end...");

    }

}

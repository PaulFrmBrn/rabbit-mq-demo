package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 24.03.15.
 */
public class Producer {

    private final static String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] args) throws IOException {

        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
                                                        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        boolean durable = true; // declaring queue to be durable - to be able to persist messages to the disk
        channel.queueDeclare(TASK_QUEUE_NAME,durable,false,false,null);  // declaring the queue

        // TODO there is still chance to loose message after it was sent to RabbitMQ and before it was stored:
        // TODO 1. there is some time between these
        // TODO 2. first message come to cache
        // TODO to force the persistence see  https://www.rabbitmq.com/confirms.html

        String message = "Hello world!.............";

        channel.basicPublish( "", TASK_QUEUE_NAME,
                MessageProperties.PERSISTENT_TEXT_PLAIN,
                message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();



    }

}

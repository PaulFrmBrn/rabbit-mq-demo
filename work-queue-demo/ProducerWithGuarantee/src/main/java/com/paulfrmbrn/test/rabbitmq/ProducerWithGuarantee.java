package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 25.03.15.
 */
public class ProducerWithGuarantee {

    private final static String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] args) throws IOException {

        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        boolean durable = true; // declaring queue to be durable - to be able to persist messages to the disk
        channel.queueDeclare(TASK_QUEUE_NAME,durable,false,false,null);  // declaring the queue

        // confirmation mechanism is in contrast to transaction mechanism but 250 times faster

        channel.confirmSelect(); // enables publisher acknowledgements on this channel - channel is in confirm mode

        String message = "Hello world!.............";

        channel.basicPublish( "", TASK_QUEUE_NAME,
                MessageProperties.PERSISTENT_TEXT_PLAIN, // persistent message
                message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");

        // TODO implement
        // Channel.setReturnListener should be implemented in order to react for uncountable message return from broker to client
        // otherwise such a message will be silently dropped

        try {
            channel.waitForConfirmsOrDie();  // Wait until all messages published since the last call have been either ack'd or nack'd by the broker
            // broker sent basic.ack (no exception)
            //
            // For routable messages, the basic.ack is sent when a message has been accepted by all the queues.
            // For persistent messages routed to durable queues, this means persisting to disk.
            // For mirrored queues, this means that all mirrors have accepted the message.
        } catch (InterruptedException e) {
            // broker sent basic.nack
            // basic.nack will only be delivered if an internal error occurs in the Erlang process responsible for a queue.
            System.out.println("something went wrong");
            // some actions to handle the situation
        }

        // NB fro Ack Latency for Persistent Messages see https://www.rabbitmq.com/confirms.html

        channel.close();
        connection.close();

    }

}

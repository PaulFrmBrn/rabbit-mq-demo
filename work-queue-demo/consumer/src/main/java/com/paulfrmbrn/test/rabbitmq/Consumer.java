package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 24.03.15.
 */
public class Consumer {

    private final static String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        boolean durable = true; // declaring queue to be durable - to be able to persist messages to the disk
        channel.queueDeclare(TASK_QUEUE_NAME,durable,false,false,null);  // declaring the queue
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        int prefetchCount = 1;
        channel.basicQos(prefetchCount); // dispatching new message to consumer only if the previous message was acknowledged

        QueueingConsumer consumer = new QueueingConsumer(channel);
        boolean autoAck = false; // auto acknowledgement is now disable. message would be deleted from the queue
                                 // only if consumer will acknowledge it
        channel.basicConsume(TASK_QUEUE_NAME, autoAck, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());

            System.out.println(" [x] Received '" + message + "'");
            doWork(message);
            System.out.println(" [x] Done" );

            // TODO try to omit this - check acknowledgement is not working
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false); // sending acknowledgement to the RabbitMQ
        }

    }

    private static void doWork(String task) throws InterruptedException {
        for (char ch: task.toCharArray()) {
            if (ch == '.') Thread.sleep(1000);
        }
    }

}

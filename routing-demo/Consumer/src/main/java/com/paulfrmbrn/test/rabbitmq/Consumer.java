package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by paulfrmbrn on 29.03.15.
 */
public class Consumer {

    private static final String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("consumer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        // direct - messages goes to queues according theirs bindings (_with_ routing)
        channel.exchangeDeclare(EXCHANGE_NAME, "direct"); // declaring exchange to send messages to
        String queueName = channel.queueDeclare().getQueue(); // creating new queue witch will get messages from the exchange

        String severitiesOneString = args[0];
        String severities[] = severitiesOneString.split(" ");
        for (String severity : severities) {
            channel.queueBind(queueName, EXCHANGE_NAME, severity); // bind the exchange and the queue _WITH_ routing by severity
        }
        System.out.println("[*] Severity levels: " + Arrays.toString(severities))   ;

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        boolean autoAck = true; // auto acknowledgement is enabled. message would be deleted from the queue just after
        // sending it to the consumer
        channel.basicConsume(queueName, autoAck, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());  // getting the message itself
            String routingKey = delivery.getEnvelope().getRoutingKey(); // getting the routing key of the message

            System.out.println(" [x] Received '" + routingKey + "':'" + message + "'");

        }



    }

}

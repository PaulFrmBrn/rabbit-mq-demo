package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by paulfrmbrn on 29.03.15.
 */
public class Producer {

    private static final String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args) throws IOException {

        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        // direct - messages goes to queues according theirs bindings (_with_ routing)
        channel.exchangeDeclare(EXCHANGE_NAME, "direct"); // declaring exchange to send messages to

        String severity = args[0]; // getting severity
        String message = args[1]; // getting message

        System.out.println("args = " + Arrays.toString(args));

        channel.basicPublish(
                EXCHANGE_NAME, // sending message to the concrete exchange
                severity, // od NOT specifying concrete queue
                null, // message is NOT persistent
                message.getBytes()
        );

        System.out.println(" [x] Sent '" + severity + "':'" + message + "'");
        System.out.println("producer end...");


        channel.close();
        connection.close();

    }

}

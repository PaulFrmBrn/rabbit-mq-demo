package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 28.03.15.
 */
public class Producer {

    private static final String EXCHANGE_NAME = "logs";

    // to see the list of exchanges call
    // sudo rabbitmqctl list_exchanges

    public static void main(String[] args) throws IOException {

        System.out.println("producer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        // fanout - broadcast messages to all queues without routing
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout"); // declaring exchange to send messages to

        String message = "Hello world!";

        channel.basicPublish(
                EXCHANGE_NAME, // sending message to the concrete exchange
                "", // od NOT specifying concrete queue
                null, // message is NOT persistent
                message.getBytes());

        System.out.println(" [x] Sent '" + message + "'");

        System.out.println("producer end...");

    }

}

package com.paulfrmbrn.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;

/**
 * Created by paulfrmbrn on 28.03.15.
 */
public class Consumer {

    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("consumer start...");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(); // abstracts the socket connection, and takes care of
        // protocol version negotiation and authentication and so on
        Channel channel = connection.createChannel(); // channel to the queue

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout"); // declaring exchange to get messages from
        String queueName = channel.queueDeclare().getQueue(); // creating new queue witch will get messages from the exchange
        channel.queueBind(queueName, EXCHANGE_NAME, ""); // bind the exchange and the queue

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        boolean autoAck = true; // auto acknowledgement is enabled. message would be deleted from the queue just after
        // sending it to the consumer
        channel.basicConsume(queueName, autoAck, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());

            System.out.println(" [x] Received '" + message + "'");
        }

    }

}
